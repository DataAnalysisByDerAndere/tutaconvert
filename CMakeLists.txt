# SPDX-License-Identifier: Apache-2.0
# tutaconvert (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert)
# Copyright 2018 - 2022 The TutaConvert Authors (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/blob/main/AUTHORS)
# Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

cmake_minimum_required(VERSION 3.0.0)
project(tutaconvert VERSION 0.1.0)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wshadow -Weffc++ -pedantic")

# add the MathFunctions library
add_subdirectory(Program_Options)

add_executable(tutaconvert main.cpp)


target_link_directories(${PROJECT_NAME} PRIVATE Program_Options)

# add the binary tree to the search path for include files
target_include_directories(${PROJECT_NAME} PUBLIC
                          "${PROJECT_BINARY_DIR}"
                          "${PROJECT_SOURCE_DIR}/Program_Options"
                          )

target_link_libraries(${PROJECT_NAME} program_options)


# configure a header file to pass some of the CMake settings
# to the source code
configure_file(tutaconvertConfig.h.in tutaconvertConfig.h)


install(TARGETS tutaconvert DESTINATION bin)
install(FILES "${PROJECT_BINARY_DIR}/tutaconvertConfig.h" DESTINATION include)


install(FILES "${PROJECT_SOURCE_DIR}/NOTICE" DESTINATION documentation)
install(FILES "${PROJECT_SOURCE_DIR}/LICENSE" DESTINATION documentation)
install(FILES "${PROJECT_SOURCE_DIR}/AUTHORS" DESTINATION documentation)
install(FILES "${PROJECT_SOURCE_DIR}/README.md" DESTINATION documentation)
install(DIRECTORY "${PROJECT_SOURCE_DIR}/LICENSES/" DESTINATION LICENSES)


include(InstallRequiredSystemLibraries)
set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
set(CPACK_RESOURCE_FILE_README "${CMAKE_CURRENT_SOURCE_DIR}/README.md")
set(CPACK_PACKAGE_VERSION_MAJOR "0")
set(CPACK_PACKAGE_VERSION_MINOR "1")
set(CPACK_PACKAGE_VERSION_PATCH "0")
set(CPACK_SOURCE_GENERATOR "ZIP")
if(WIN32 AND NOT UNIX)
  # There is a bug in NSI that does not handle full UNIX paths properly.
  # Make sure there is at least one set of four backlashes.
  set(CPACK_NSIS_HELP_LINK "https:\\\\\\\\gitlab.com/DataAnalysisByDerAndere\\\\ReproducibleWorkflowsByDerAndere\\\\tutaconvert\\\\-\\\\issues")
  set(CPACK_NSIS_URL_INFO_ABOUT "https:\\\\\\\\gitlab.com/DataAnalysisByDerAndere\\\\ReproducibleWorkflowsByDerAndere\\\\tutaconvert")
  set(CPACK_NSIS_CONTACT "DerAndere")
  set(CPACK_NSIS_MODIFY_PATH ON)
  set(CPACK_NSIS_BRANDING_TEXT "Copyright 2022 DerAndere")
else()
  set(CPACK_STRIP_FILES "bin/tutaconvert.exe")
  set(CPACK_SOURCE_STRIP_FILES "")
endif()
include(CPack)
