/**
 + SPDX-License-Identifier: Apache-2.0
 * tutaconvert (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert)
 * Copyright 2018 - 2022 The TutaConvert Authors (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/blob/main/AUTHORS)
 * Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Based on:
 * dog (https://github.com/mostsignificant/dog/tree/method-diy)
 * Copyright 2018 Christian Göhring
 * SPDX-License-Identifier: LicenseRef-MIT_dog
 */

/**
 * @file program_options.cpp
 * @authors Christian Göhring, DerAndere
 * @copyright 2018 - 2022 The TutaConvert Authors (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/blob/main/AUTHORS)
 * Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues)
 * Based on (with modifications): https://github.com/mostsignificant/dog/blob/586359f967507c3b393c6d454c2a1753edab4265/src/program_options.cpp
 * Copyright 2018 Christian Göhring
 */

#include "program_options.h"

#include <filesystem>
#include <stdexcept>
#include <vector>

namespace {

std::filesystem::path _input_file;
std::filesystem::path _output_file;

}  // namespace

void program_options::parse(int argc, char* argv[]) {

    const std::vector<std::string_view> args(argv + 1, argv + argc);

    // argv[4] is the program name.
    if (argc != 5) {
      throw std::runtime_error("tutaconvert: Wrong number of arguments. Usage: tutaconvert -i <input_file> -o <output_file>");
    }

    for (int idx = 0; idx < 4; idx++) {
        if (args[idx] == "-i" || args[idx] == "--infile") {
            if (!std::filesystem::exists(args[idx + 1])) {
                throw std::runtime_error(std::string("tutaconvert: ") + std::string(args[idx + 1]) + ": No such file or directory");
            }
            else {
                _input_file = std::filesystem::path(args[idx + 1]);
            }
        }
        if (args[idx] == "-o" || args[idx] == "--outfile") {
            if (std::filesystem::exists(args[idx + 1])) {
                throw std::runtime_error(std::string("tutaconvert: ") + std::string(args[idx + 1]) + ": output file already exists");
            }
            else {
                _output_file = std::filesystem::path(args[idx + 1]);
            }
        }
    }
}

std::filesystem::path program_options::ifile() {
    return _input_file;
}

std::filesystem::path program_options::ofile() {
    return _output_file;
}